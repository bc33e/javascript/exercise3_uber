const UBER_CAR = "uberCar";
const UBER_SUV = "uberSUV";
const UBER_BLACK = "uberBlack";
function tinhTienKmDauTien(car) {
  if (car == UBER_CAR) {
    return 8000;
  }
  if (car == UBER_SUV) {
    return 9000;
  }
  if (car == UBER_BLACK) {
    return 10000;
  }
  return 0;
}

function tinhTienKm1Den19(car) {
  switch (car) {
    case UBER_CAR: {
      return 7500;
    }
    case UBER_SUV: {
      return 8500;
    }
    case UBER_BLACK: {
      return 9500;
    }
    default:
      return 0;
  }
}

function tinhTienHonKm19(car) {
  switch (car) {
    case UBER_CAR: {
      return 7000;
    }
    case UBER_SUV: {
      return 8000;
    }
    case UBER_BLACK: {
      return 9000;
    }
    default:
      return 0;
  }
}

function tinhTienUber() {
  var carOption = document.querySelector(
    'input[name="selector"]:checked'
  ).value;
  console.log("carOption: ", carOption);
  var giaKmDauTien = tinhTienKmDauTien(carOption);
  console.log("giaKmDauTien: ", giaKmDauTien);

  var giaKm1Den19 = tinhTienKm1Den19(carOption);
  console.log("giaKm1Den19: ", giaKm1Den19);

  var giaHonKm19 = tinhTienHonKm19(carOption);
  console.log("giaHonKm19: ", giaHonKm19);

  var soKmDi = document.getElementById("txt-km").value * 1;
  var tienPhaiTra = 0;
  if (soKmDi <= 1) {
    tienPhaiTra = giaKmDauTien * soKmDi;
  } else if (soKmDi > 1 && soKmDi <= 19) {
    tienPhaiTra = giaKmDauTien + (soKmDi - 1) * giaKm1Den19;
  } else {
    tienPhaiTra = giaKmDauTien + 18 * giaKm1Den19 + (soKmDi - 19) * giaHonKm19;
  }
  console.log("Tiền phải trả: ", tienPhaiTra);
  document.getElementById("divThanhTien").style.display = "block";
  document.getElementById(
    "divThanhTien"
  ).innerHTML = `<div style="display:block">Tiền phải thanh toán: ${tienPhaiTra.toLocaleString()} VND</div>`;
}
